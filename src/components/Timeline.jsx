import React, {useContext} from 'react';
import Skeleton from "react-loading-skeleton";
import usePhotos from "../hooks/use-photos";
import Post from "./post/Post";
import LoggedInUserContext from "../context/logged-in-user";

export default function Timeline() {
    const { user } = useContext(LoggedInUserContext)
    // we need to get the logged in user's photos (hook use-photos)
    const { photos } = usePhotos(user);
    //on oading the photos, we need to use react skeleton
    // render them with post comment
    // no photos suggest to create photos
    //console.log('photos', photos)
    return (
        <div className="container col-span-2">
            <p> TIMELINE </p>
            {!photos ? (
                <Skeleton count={3} width={640} height={500} className={"mb-5"}/>
                ) : photos?.length > 0 ? (
                    photos.map((content) => <Post key={content.docId} content={content} />)
                ) : (
                    <p className="text-center text-2xl">Follow people to see photos</p>
                )
            }
        </div>
        
    )
}
