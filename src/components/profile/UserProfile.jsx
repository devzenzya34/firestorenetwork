import React from 'react';
import {useReducer, useEffect} from 'react';
import PropTypes from 'prop-types';
import HeaderProfile from './HeaderProfile';
import {getUserPhotosByUsername} from "../../services/firebase";
import Photos from "./Photos";


function UserProfile({user}) {

    const reducer = (state, newState) => ({...state, ...newState})
    const initialState = {
        profile: {},
        photosCollection: [],
        followerCount: 0
    };
    const [{profile, photosCollection, followerCount}, dispatch] = useReducer(
        reducer,
        initialState
    );

    useEffect(() => {
        async function getProfileInfoAndPhotos() {
            //const [user] = await getUserByUsername(username);
            const photos = await getUserPhotosByUsername(user.username);
            dispatch({profile: user, photosCollection: photos, followerCount: user.followers.length});
        }
        if (user.username) {
            getProfileInfoAndPhotos();
        }

    }, [user]);

    return (
        <>
            <HeaderProfile
                photosCount={photosCollection ? photosCollection.length : 0}
                profile={profile}
                followerCount={followerCount}
                setFollowerCount={dispatch}
            />
            <Photos photos={photosCollection} />
        </>
    );
}

export default UserProfile;

UserProfile.propTypes = {
    user: PropTypes.shape({
        dateCreated: PropTypes.number,
        emailAddress: PropTypes.string,
        followers: PropTypes.array,
        following: PropTypes.array,
        fullName: PropTypes.string,
        userId: PropTypes.string,
        username: PropTypes.string
    })
};