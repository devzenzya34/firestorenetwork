import Firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

//import { seedDatabase } from '../seed'

//ICI on approvisionne base firestore UNE fois pour remplir la base de données en important le seed.js!
const config = {
    apiKey: "AIzaSyCbTDNtQgmu3FgKLxCg7OL8h8PmheXuklc",
    authDomain: "instawebapp-171c5.firebaseapp.com",
    projectId: "instawebapp-171c5",
    storageBucket: "instawebapp-171c5.appspot.com",
    messagingSenderId: "378780849930",
    appId: "1:378780849930:web:338b28d6e3226399e84cf3",
    measurementId: "G-FNR31HT41J"

}

const firebase = Firebase.initializeApp(config);
const { FieldValue } = Firebase.firestore;

//call Seed.js
//seedDatabase(firebase)

export { firebase, FieldValue};