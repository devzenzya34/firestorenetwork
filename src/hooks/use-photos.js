import { useState, useEffect} from 'react'
//import UserContext from "../context/user";
import { getPhotos,
    //getUserByUserId
} from "../services/firebase";

function usePhotos(user) {
    const [photos, setPhotos] = useState(null)
    // const {
    //     user: { uid: userId = ''}
    // } = useContext(UserContext);

    // useEffect(() => {
    //     async function getTimelinePhotos() {
    //         const [{ following }] = await getUserByUserId(userId);
    //         let followedUserPhotos = [];
    //
    //         //does the use actually follow people?
    //         if(following.length > 0) {
    //             followedUserPhotos = await getPhotos(userId, following);
    //         }
    //         followedUserPhotos.sort((a, b) => b.dateCreated - a.dateCreated);
    //         setPhotos(followedUserPhotos);
    //     }
    //
    //     getTimelinePhotos()
    //
    // }, [userId])
    //
    useEffect(() => {
        async function getTimelinePhotos() {
            // example: [2, 1, 5] <- 2 being raphel
            if (user?.following?.length > 0) {
                const followedUserPhotos = await getPhotos(user.userId, user.following);
                // re-arrange array to be newest photos first by dateCreated
                followedUserPhotos.sort((a, b) => b.dateCreated - a.dateCreated);
                setPhotos(followedUserPhotos);
            }
        }
        getTimelinePhotos();
    }, [user?.userId, user.following]);
    return {photos};
}

export default usePhotos;